# XSIG protocol

This package code and decode data in XSIG protocol format, as defined in Crestron SIMPL Windows manual.
Module within SIMPL Windows SDK called ISC (Inter System Communications).

What this package actually do:

* Unpack received bytes
* and pack signals to bytes for sending.

Tested with Crestron RMC3, test files in `examples`.


BSD-3-Clause