# -*- coding: utf-8 -*-

import sys
import socket

import xsig

__author__ = "d41"


def test(controller_address):
    """ Проверка работы разных сигналов.
    Общается с программой для RMC3 и пересылает несколько типов сигналов.
    Также получает от него некоторые на разбор.

    :param: controller_address: tuple(ip_address, port)
    :return: None
    """

    state = False

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect(controller_address)
        s.settimeout(0.05)
        while True:
            try:
                data = s.recv(1024)
                print(repr(data))
                for signal in xsig.unpack(data):
                    print(signal)

                state = not state
                sig = xsig.DigitalSignal(index=4, value=state)
                message = xsig.pack(sig)
                s.sendall(message)
                print(state, sig, message)
            except KeyboardInterrupt:
                break
            finally:
                print("Connection terminated")


def test_interactive(controller_address):
    """ Типа проверка пересылки данных от ПК к которому подключены рабочие места.
    Спрашивает кого включить. Сначала отправляет команду на выключение предыдущего.
    Потом на включение требуемого.

    :param: controller_address: tuple(ip_address, port)
    :return: None
    """

    mic = 0

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect(controller_address)
        s.settimeout(0.05)
        while True:
            try:
                text = input("WP:")
                signal = xsig.DigitalSignal(index=mic, value=False)
                s.send(xsig.pack(signal))
                mic = int(text)
                signal = xsig.DigitalSignal(index=mic, value=True)
                s.send(xsig.pack(signal))
            except KeyboardInterrupt:
                break


if "__main__" == __name__:
    if "-i" in sys.argv:
        test_interactive(("10.0.0.1", 4002))
    else:
        test(("10.0.0.1", 4001))
