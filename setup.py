# -*- coding: utf-8 -*-

import setuptools

__author__ = "d41"

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="xsig-d41",
    version="0.1.0",
    author="d41",
    author_email="defunct41@gmail.com",
    description="XSIG protocol codec",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/d41/xsig",
    license="BSD-3-Clause",
    packages=setuptools.find_packages(),
    classifiers=[
        "Development Status :: 4 - Beta",
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: BSD License",
        "Operating System :: OS Independent",
        "Topic :: Scientific/Engineering :: Interface Engine/Protocol Translator",

    ],
    python_requires='>=3.6',
)
