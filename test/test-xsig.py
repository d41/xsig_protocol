# -*- coding: utf-8 -*-

import unittest

import xsig

__author__ = "d41"

serial_packets = [
    b'\xc8\x00Hello World\xff',
    b'\xc8\x00Test Line\xff',
    b'\xc8\x01Some random info\xff',
    b'\xc8\x01\xcf\xf0\xe8\xe2\xe5\xf2 1234\xff',
    b'\xc8\x00Text\xfe\x01Line\xfe\x00End\xff',
]

serial_signals = [
    xsig.SerialSignal(index=0, value=bytearray(b'Hello World')),
    xsig.SerialSignal(index=0, value=b"Test Line"),
    xsig.SerialSignal(index=1, value=b"Some random info"),
    xsig.SerialSignal(index=1, value=b"\xcf\xf0\xe8\xe2\xe5\xf2 1234"),
    xsig.SerialSignal(index=0, value=b"Text\xFFLine\xFEEnd"),
]

analog_packets = [
    b'\xc0\x02\x00\x08\xe0\x03\x00`',
    b'\xc0\x02\x002',
    b'\xd0\x03A\x00',
    b'\xc0\x02\x00\x08',
    b'\xe0\x03\x00`',
]

analog_signals = [
    [xsig.AnalogSignal(2, 0x08), xsig.AnalogSignal(3, 0x8060)],
    xsig.AnalogSignal(2, 0x32),
    xsig.AnalogSignal(3, 0x6080),
    xsig.AnalogSignal(2, 0x08),
    xsig.AnalogSignal(3, 0x8060),
]

digital_packets = [
    b'\x80\x04',
    b'\xa0\x04',
    b'\x80\x05',
    b'\xa0\x05',
]
digital_signals = [
    xsig.DigitalSignal(4, True),
    xsig.DigitalSignal(4, False),
    xsig.DigitalSignal(5, True),
    xsig.DigitalSignal(5, False),
]


class XSIGPackTestCase(unittest.TestCase):
    def test_serial(self):
        for packet, signal in zip(serial_packets, serial_signals):
            self.assertEqual(packet, xsig.pack(signal))

    def test_analog(self):
        for packet, signal in zip(analog_packets, analog_signals):
            self.assertEqual(packet, xsig.pack(signal))

    def test_digital(self):
        for packet, signal in zip(digital_packets, digital_signals):
            self.assertEqual(packet, xsig.pack(signal))

    def test_mixed(self):
        packets = zip(digital_packets, analog_packets, serial_packets)
        signals = zip(digital_signals, analog_signals, serial_signals)

        for packet, signal in zip(next(packets), next(signals)):
            self.assertEqual(packet, xsig.pack(signal))


class XSIGUnpackTestCase(unittest.TestCase):
    def test_serial(self):
        for packet, signal in zip(serial_packets, serial_signals):
            new_signal = xsig.unpack(packet)
            if len(new_signal) == 1:
                new_signal = new_signal[0]
                self.assertEqual(new_signal.Index, signal.Index)
                self.assertEqual(new_signal.Value, signal.Value)
            else:
                for ns, s in zip(new_signal, signal):
                    self.assertEqual(ns.Index, s.Index)
                    self.assertEqual(ns.Value, s.Value)

    def test_analog(self):
        for packet, signal in zip(analog_packets, analog_signals):
            new_signal = xsig.unpack(packet)
            if len(new_signal) == 1:
                new_signal = new_signal[0]
                self.assertEqual(new_signal.Index, signal.Index)
                self.assertEqual(new_signal.Value, signal.Value)
            else:
                for ns, s in zip(new_signal, signal):
                    self.assertEqual(ns.Index, s.Index)
                    self.assertEqual(ns.Value, s.Value)

    def test_digital(self):
        for packet, signal in zip(digital_packets, digital_signals):
            new_signal = xsig.unpack(packet)
            if len(new_signal) == 1:
                new_signal = new_signal[0]
                self.assertEqual(new_signal.Index, signal.Index)
                self.assertEqual(new_signal.Value, signal.Value)
            else:
                for ns, s in zip(new_signal, signal):
                    self.assertEqual(ns.Index, s.Index)
                    self.assertEqual(ns.Value, s.Value)

    def test_mixed(self):
        packets = zip(digital_packets, analog_packets, serial_packets)
        signals = zip(digital_signals, analog_signals, serial_signals)

        for packet, signal in zip(next(packets), next(signals)):
            new_signal = xsig.unpack(packet)
            if len(new_signal) == 1:
                new_signal = new_signal[0]
                self.assertEqual(new_signal.Index, signal.Index)
                self.assertEqual(new_signal.Value, signal.Value)
            else:
                for ns, s in zip(new_signal, signal):
                    self.assertEqual(ns.Index, s.Index)
                    self.assertEqual(ns.Value, s.Value)


if __name__ == '__main__':
    unittest.main()
