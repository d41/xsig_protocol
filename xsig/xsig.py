# -*- coding: utf-8 -*-

from copy import copy

__author__ = "d41"
__doc__ = """\
XSIG protocol as defined in Crestron SIMPL Windows manual.
Module within SIMPL Windows SDK called ISC (Inter System Communications).

Unpack received bytes and pack signals to bytes for sending.
"""


class Signal:
    def __init__(self, index, value):
        self.Index = index
        self.Value = value

    def __str__(self):
        return "{} [{}] = {}".format(self.__class__.__name__, self.Index, self.Value)

    def pack(self):
        pass

    @staticmethod
    def unpack(packet):
        pass

    @staticmethod
    def check_header(byte):
        pass


class DigitalSignal(Signal):
    def __init__(self, index, value):
        super().__init__(index, value)

    def pack(self):
        packet = bytes()

        index_hi = (self.Index & 0x0F80) >> 7
        index_lo = (self.Index & 0x007F)

        value = 0 if self.Value else 0x20

        packet += bytes([0x80 + value + index_hi, index_lo])

        return packet

    @staticmethod
    def unpack(packet):
        byte_a, byte_b = packet

        index = ((byte_a & 0x1F) << 7) + byte_b
        value = not (byte_a & 0x20 == 0x20)

        return DigitalSignal(index, value)

    @staticmethod
    def check_header(byte):
        descriptor = (byte & 0x80) == 0x80
        digital = (byte & 0x40) == 0x00
        return descriptor and digital


class AnalogSignal(Signal):
    def __init__(self, index, value):
        super().__init__(index, value)

    def pack(self):
        packet = bytes()

        index_hi = (self.Index & 0x0380) >> 7
        index_lo = (self.Index & 0x007F)

        value_a = (self.Value & 0xC000) >> (14 - 4)
        value_c = (self.Value & 0x007F)
        value_b = (self.Value & 0x3F80) >> 7

        packet += bytes([0xC0 + value_a + index_hi, index_lo, value_b, value_c])

        return packet

    @staticmethod
    def unpack(packet):
        byte_a, byte_b, byte_c, byte_d = packet

        index = ((byte_a & 0x07) << 7) + byte_b
        value = (byte_a & 0x30) >> 4
        value = (value << 7) + byte_c
        value = (value << 7) + byte_d

        return AnalogSignal(index, value)

    @staticmethod
    def check_header(byte):
        descriptor = (byte & 0x80) == 0x80
        analog_or_serial = (byte & 0x40) == 0x40
        analog = (byte & 0x08) == 0x00
        return descriptor and analog_or_serial and analog


class SerialSignal(Signal):
    def __init__(self, index, value):
        super().__init__(index, value)

    def pack(self):
        packet = bytes()

        index_hi = (self.Index & 0x0380) >> 7
        index_lo = (self.Index & 0x007F)

        value = self.Value.replace(b"\xFE", b"\xFE\x00").replace(b"\xFF", b"\xFE\x01")
        packet += bytes([0xC8 + index_hi, index_lo]) + value + b"\xff"

        return packet

    @staticmethod
    def unpack(packet):
        byte_a, byte_b = packet[:2]

        index = ((byte_a & 0x07) << 7) + byte_b

        last = packet.index(b"\xFF")
        value = packet[2:last].replace(b"\xFE\x01", b"\xFF").replace(b"\xFE\x00", b"\xFE")

        return SerialSignal(index, value)

    @staticmethod
    def check_header(byte):
        descriptor = (byte & 0x80) == 0x80
        analog_or_serial = (byte & 0x40) == 0x40
        serial = (byte & 0x08) == 0x08
        return descriptor and analog_or_serial and serial


def pack(signals: list) -> bytes:
    packet = bytes()
    if not (type(signals) is list):
        signals = [signals]

    for signal in signals:
        packet += signal.pack()

    return packet


def unpack(packet: bytes) -> list:
    buffer = copy(packet)
    signals = list()

    while len(buffer):
        test = buffer[0]
        if DigitalSignal.check_header(test):
            signals.append(DigitalSignal.unpack(buffer[:2]))
            buffer = buffer[2:]
        elif AnalogSignal.check_header(test):
            signals.append(AnalogSignal.unpack(buffer[:4]))
            buffer = buffer[4:]
        elif SerialSignal.check_header(test):
            last = buffer.index(b"\xFF")
            signals.append(SerialSignal.unpack(buffer[:last + 1]))
            buffer = buffer[last + 1:]
        else:
            buffer = buffer[1:]
    return signals
